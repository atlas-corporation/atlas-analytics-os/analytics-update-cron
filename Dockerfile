FROM nodered/node-red
COPY flows.json /data/flows.json

CMD ["npm","start","--","--userDir","/data"]
