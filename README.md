# analytics-update-cron

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

*  `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

*  `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* **YOU ARE HERE** `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

*  `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About analytics-update-cron

This is a node red microservice used as a cron job which triggers heavy queries within the `analytics-update` microservice. These endpoints serve as a caching layer for the `atlas-analytics-app` react front end. This microservice contains a visual cron job - the nodered front end - so that users can more easily visualize the process of generating the cache for the reporting application.


## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t analytics-update-cron . && docker run analytics-update-cron`. It should probably be run in concert with the `analytics-update` microservice as it is meant to work in concert with its cron job microservice. In that case use `docker-compose build && docker-compose up -d` to execute.